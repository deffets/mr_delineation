
import React, { Component } from "react";
import Layout from './layout'
import Link from "next/link"

export default class AdminLayout extends React.Component {
  constructor(props) {
    super(props);
  }
  
  render() {
    return (
      <Layout admin={true}>
        <div className='mainBlock'>
          <nav>
            <ul>
              <li>
                <Link href={{ pathname: "/admin/manageWorkflows" }}>
                  <a>Manage workflows</a>
                </Link>
              </li>
              <li>
                <Link href={{ pathname: "/admin/addUser"}}>
                  <a>Add users</a>
                </Link>
              </li>
              <li>
                <Link href={{ pathname: "/admin/manageKeys"}}>
                  <a>SSH keys</a>
                </Link>
              </li>
            </ul>
          </nav>
          <div className="tabcontent">
            {this.props.children}
          </div>
        </div>
        
        <style>{`
          .mainBlock{
            height: 100%;
            width: 100%;
            position: relative;
            display: flex;
            flex-direction: column;
          }
          .mainBlock nav {
            height: 40px;
            width: 100%;
            padding: 0 0 0 0;
            position: relative;
            background-color: #697382;
            justify-content: center;
          }
          .mainBlock nav ul {
            height: 100%;
            width: 100%;
            position: relative;
            display: flex;
            list-style: none;
            margin-left: 0;
            padding: 0 0 0 0;
            justify-content: center;
          }
          .mainBlock li {
            padding-right: 20px;
            text-align: center;
            justify-content: center;
          }
          .mainBlock a {
            color: #ffffff;
            text-decoration: none;
            text-align: center;
          }
          .tabcontent {
            height: calc(100% - 40px);
            width: 100%;
            padding: 0 0 0 0;
            background-color: #ffffff;
            position: relative;
          }
        `}</style>
      </Layout>
    );
  }
}

