import Head from 'next/head'
import Header from './header'

const Layout = (props) => {
  return(
  <>
    <div className="layoutMain">
      <Head>
        <title>CHARP</title>
      </Head>

      <Header admin={props.admin}/>

      <main>
        {props.children}
      </main>
    </div>

    <style>{`
      html, body {
        margin: 0;
	      padding: 0;
	      border: 0;
	      font-size: 100%;
	      vertical-align: baseline;
        height: 100vh;
        width: 100vw;
        box-sizing: border-box;
        /*-webkit-font-smoothing: antialiased;*/
        -moz-osx-font-smoothing: grayscale;
        font-family: Roboto, OpenSans, HelveticaNeue-Light, Helvetica Neue Light,
          Helvetica Neue, Helvetica, Arial, Lucida Grande, sans-serif;
        position: relative;
        background-color: #ffffff;
      }
      .layoutMain {
        height: 100vh;
        width: 100vw;
        display: block;
        position: relative;
      }
      .layoutMain header {
        width: 100vw;
        height: 70px;
        position: relative;
      }
      main {
        height: calc(100vw - 70px);
        width: 100%;
        background-color: #ffffff;
        position: relative;
      }
    `}</style>
  </>)
}

export default Layout
