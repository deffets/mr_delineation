import Link from 'next/link'
import { useUser } from '../lib/hooks'
import dynamic from "next/dynamic";

const Header = (props) => {
  const user = useUser()

  return (
    <header>
      <nav>
        <ul>
          <li>
            <Link href="/">
              <a>Home</a>
            </Link>
          </li>
          {user ? (
            <>
              <li>
                <Link href="/profile">
                  <a>Profile</a>
                </Link>
              </li>
              {props.admin ? (
                <li>
                  <Link href="/admin">
                    <a>Admin</a>
                  </Link>
                </li>
              ) : (null)}
              <li>
                <a href="/api/logout">You are currently logged in as: {user.username} > Logout</a>
              </li>
            </>
          ) : (
            <li>
              <Link href="/login">
                <a>Login</a>
              </Link>
            </li>
          )}
        </ul>
      </nav>
      <style>{`
        header nav {
          max-width: 42rem;
          margin: 0 auto;
          padding: 0.2rem 1.25rem;
        }
        header ul {
          display: flex;
          list-style: none;
          margin-left: 0;
          padding-left: 0;
        }
        header li {
          margin-right: 1rem;
        }
        header li:first-child {
          margin-left: auto;
        }
        header a {
          color: #fff;
          text-decoration: none;
        }
        header {
          color: #fff;
          background-color: #333;
        }
      `}</style>
    </header>
  )
}

export default Header
