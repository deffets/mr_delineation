import { MongoClient, ObjectId } from 'mongodb'
import fs from 'fs'
import path from 'path'
import uniqBy from 'lodash/uniqBy'
import os from "os"


let uri = process.env.MONGODB_URI
let dbName = process.env.MONGODB_DB

let cachedClient = null
let cachedDb = null

if (!uri) {
  throw new Error(
    'Please define the MONGODB_URI environment variable inside .env.local'
  )
}

if (!dbName) {
  throw new Error(
    'Please define the MONGODB_DB environment variable inside .env.local'
  )
}


export async function addPublicKey(key) {
  const d = new Date();
  const n = d.getTime().toString();
  
  const homedir = require('os').homedir();
  
  const src = path.join(homedir, ".ssh", "authorized_keys");
  const dest = path.join(homedir, ".ssh", "authorized_keys_saved" + n);

  try {
    await fs.copyFileSync(src, dest); // make a backup
  } catch(e) {
    await fs.writeFileSync(src, "");
  }
  
  await fs.appendFileSync(src, key);
}


export async function cleanUnsafe() {
  const {db} = await connectToDatabase();
  
  let count = 0;
  
  let dataIds = await db.collection("data").find({}, {"seriesId": 1}).toArray();
  dataIds = uniqBy(dataIds.filter(elem => elem.seriesId), "seriesId").map(el => el.seriesId);
  
  for (let i=0 ; i<dataIds.length; i++) {
    const wfIds = await db.collection("workflows").find({"seriesId": dataIds[i]}).toArray();
    
    if (!wfIds.length) {
      const instanceMeta = await db.collection("data").find({"seriesId": dataIds[i]}).toArray();
      const filePath = path.join(instanceMeta[0].dataPath, dataIds[i])
      
      let dataCount = await db.collection("data").find({"seriesId": dataIds[i]}).toArray();
      db.collection("data").deleteMany({"seriesId": dataIds[i]})
      
      for (let j=0; j<instanceMeta.length; j++) {
        try{
         fs.unlinkSync(path.join(filePath, instanceMeta[j].instanceId+'.raw'))
        } catch(e){}
        try{
          fs.unlinkSync(path.join(filePath, instanceMeta[j].instanceId+'.txt'))
        } catch(e){}
      }
      
      fs.rmdir(filePath, () => {});
      
      count += dataCount.length;
    }
  }
  
  //TODO SEGs should have a better way to be identified
  dataIds = await db.collection("data").find({"Modality": "RTSTRUCT"}, {"_id": 1}).toArray();
  dataIds = dataIds.map(el => el._id);
  
  for (let i=0 ; i<dataIds.length; i++) {
    const wfIds = await db.collection("workflows").find({"segId": dataIds[i].toString()}).toArray();
    
    if (!wfIds.length) {
      db.collection("data").deleteOne({"_id": ObjectId(dataIds[i])});
      count++;
    }
  }
  
  return count;
}


export async function connectToDatabase() {
  if (cachedClient && cachedDb) {
    return { client: cachedClient, db: cachedDb }
  }

  const client = await MongoClient.connect(uri, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })

  const db = await client.db(dbName)

  cachedClient = client
  cachedDb = db

  return { client, db }
}


export async function deleteWorkflow(entryId) {
  const {db} = await connectToDatabase();
  
  db.collection("workflows").deleteMany({"workflowId": entryId})
}


export async function getAllWorkflowEntries() {
  const {db} = await connectToDatabase();
  
  const res = await db.collection("workflows").find();
  
  return await res.toArray();
}


export async function getAllWorkflows() {
  const {db} = await connectToDatabase();
  
  const myquery = { "ready": "False" };
  const res = await db.collection("workflows").find({"parents": []});
  
  return await res.toArray();
}


export async function getSegMetaData(segId) {
  console.log('getSegMetaData ' + segId.toString())
  
  const {db} = await connectToDatabase();
    
  const segMeta = await db.collection("data").findOne({"_id": new ObjectId(segId)})
  
  segMeta.StudyInstanceUID = "NA";
  segMeta.SeriesInstanceUID = segId;
  segMeta.SOPInstanceUID = segId;
  
  return segMeta
}


export async function getSeriesData(seriesId, instanceId) {
  const seriesIdString = seriesId.toString()
  const instanceIdString = instanceId.toString()
  
  console.log('getSeriesData ' + instanceIdString)
  
  const {db} = await connectToDatabase();
  
  const instanceMeta = await db.collection("data").findOne({"instanceId": instanceIdString})
  
  const filePath = path.join(instanceMeta.dataPath, seriesIdString)
  const rawData = await fs.readFileSync(path.join(filePath, instanceIdString+'.raw'))
  
  return rawData
}


export async function getSeriesMetaData(seriesId) {
  const seriesIdString = seriesId.toString()
  
  console.log('getSeriesMetaData ' + seriesIdString)
  
  const {db} = await connectToDatabase();
    
  const ctMeta = await db.collection("data").find({"seriesId": seriesIdString})
  const meta = await ctMeta.toArray()
  
  for (let j=0; j<meta.length; j++) {
    meta[j].StudyInstanceUID = "NA";
    meta[j].SeriesInstanceUID = meta[j].seriesId;
  }
  
  return meta
}


export async function getUser(username) {
  const {db} = await connectToDatabase();
  
  const res = await db.collection("users").findOne({username});
  
  return res;
}


export async function getWorkflow(workflowId) {
  const {db} = await connectToDatabase();
  
  const workflow = await db.collection("workflows").find({"workflowId": workflowId.toString()}).toArray();
  
  return workflow;
}


export async function getWorkflowEntries(username) {
  const {db} = await connectToDatabase();
  
  const myquery = { "ready": "False" };
  const res = await db.collection("workflows").find({users: username})
  
  return await res.toArray();
}


export async function getWorkflowEntry(id) {
  const {db} = await connectToDatabase();
  
  const workflow = await db.collection("workflows").findOne({"_id": new ObjectId(id)})

  return workflow
}


export async function getWorkflowEntryData(id) {
  const {db} = await connectToDatabase();
  
  const workflow = await db.collection("workflows").findOne({"_id": new ObjectId(id)})
  
  let meta = []
  
  for (let i=0; i<workflow.seriesId.length; i++) {
    const ctMeta = await db.collection("data").find({"seriesId": workflow.seriesId[i].toString()})
    let newMeta = await ctMeta.toArray();
    
    
    for (let j=0; j<newMeta.length; j++) {
      newMeta[j].StudyInstanceUID = "NA";
      newMeta[j].SeriesInstanceUID = newMeta[j].seriesId;
    }
    
    meta = meta.concat(newMeta)
  }
  
  if (workflow.segId.length) {
    let segMeta = await db.collection("data").findOne({"_id": new ObjectId(workflow.segId)});
      
    segMeta.StudyInstanceUID = "NA";
    segMeta.SeriesInstanceUID = workflow.segId;
    segMeta.SOPInstanceUID = workflow.segId;
      
    meta = meta.concat(segMeta);
  }
  
  return meta;
}


export async function insertUser(username, salt, hash, role) {
  const {db} = await connectToDatabase();
  
  const user = {
    username,
    salt,
    hash,
    role
  };
  
  await db.collection("users").insertOne(user);
}


export async function setSegMetaData(metaData) {
  console.log('setSegMetaData ')
  
  const {db} = await connectToDatabase();
  
  let initialSegMeta = await db.collection("data").findOne({"_id": new ObjectId(metaData.SOPInstanceUID)})
  
  for (let segNb=0; segNb<initialSegMeta.ROIContourSequence.length; segNb++) {
    console.log(metaData.ROIContourSequence[segNb].ContourSequence)
    if(metaData.ROIContourSequence[segNb].changed) {
      console.log('Setting new data for segment ' + segNb + ' with a sequence of ' +  metaData.ROIContourSequence[segNb].ContourSequence.length)
      initialSegMeta.ROIContourSequence[segNb].ContourSequence = metaData.ROIContourSequence[segNb].ContourSequence.slice()
    }
  }
  
  delete initialSegMeta._id
  initialSegMeta.updated = true
  
  await db.collection("data").updateOne({"_id": new ObjectId(metaData.SOPInstanceUID)}, { $set: initialSegMeta })
  
  //const metaCopy = initialSegMeta
  //delete metaCopy._id
  //await db.collection("data").insertOne(metaCopy)
}

export async function checkAccess(username, contentType, contentId) {
  let res;
  let dbRes;
  const {db} = await connectToDatabase();
  
  const user = await db.collection("users").findOne({"username": username})
  if (user.role=="admin")
    return true;  

  switch (contentType) {
    case "clean":
      // only if admin
      res = false;
      break
    case "deleteEntry":
      // only if admin
      res = false;
      break
    case "insertUser":
      // only if admin
      res = false;
      break
    case "workflow":
      dbRes = await db.collection("workflows").findOne({"_id": new ObjectId(contentId), users: username})
      res = dbRes ? true : false
      
      if (!res)
        console.log(username + ' tried to acess workflow ' + contentId);
      
      break
    case "series":
      dbRes = await db.collection("workflows").findOne({"seriesId": contentId, users: username})
      res = dbRes ? true : false
      
      if (!res)
        console.log(username + ' tried to acess series ' + contentId);
      break
    case "seg":
      dbRes = await db.collection("workflows").findOne({"segId": new ObjectId(contentId), users: username})
      res = dbRes ? true : false
      
      if (!res)
        console.log(username + ' tried to acess seg ' + contentId);
      break
    case "publicKey":
      // only if admin
      res = false;
    default:
      res = false;
  }
  
  return res;
}

