
import { getSession } from '../../lib/iron'
import { getWorkflow, getWorkflowEntryData, getSeriesData, getSeriesMetaData, getSegMetaData, setSegMetaData, checkAccess } from '../../utils/db'

export default async function (req, res) {
  //TODO: Check for authorization
  let data = null
  
  let session = null;
  
  try {
    session = await getSession(req)
  }
  catch (e) {
    res.status(401).send(JSON.stringify(""));
    return;
  }
  
  if (!session) {
    res.status(401).send(JSON.stringify("User not authenticated"));
    return;
  }
  
  const username = session.username;
  
  if(req.query.hasOwnProperty("workflowEntryId")){
    const ok = await checkAccess(username, "workflow", req.query.workflowEntryId)
    
    if (!ok) {
      res.status(401).send(JSON.stringify("Wrong username"));
      return;
    }
    
    data = await getWorkflowEntryData(req.query.workflowEntryId)
    
    res.status(200).send(JSON.stringify(data));
    return;
  }
  
  if(req.query.hasOwnProperty("workflowId")){
    const ok = await checkAccess(username, "workflow", req.query.workflowId)
    
    if (!ok) {
      res.status(401).send(JSON.stringify("Wrong username"));
      return;
    }
    
    data = await getWorkflow(req.query.workflowId)
    
    res.status(200).send(JSON.stringify(data));
    return;
  }
      
  if (req.query.hasOwnProperty("seriesId")){
    const ok = await checkAccess(username, "series", req.query.seriesId)
    
    if (!ok) {
      res.status(401).send(JSON.stringify("Wrong username"));
      return;
    }
      
    if (req.query.hasOwnProperty("metaData")){
      data = await getSeriesMetaData(req.query.seriesId)
      res.status(200).send(JSON.stringify(data));
      return;
    }
    else if (req.query.hasOwnProperty("instanceId")) {
      data = await getSeriesData(req.query.seriesId, req.query.instanceId)
      res.status(200).send(Buffer.from(data).fill(data))
      return
    }
  }
  
  if (req.query.hasOwnProperty("segId")){
    const ok = await checkAccess(username, "seg", req.query.segId)
    
    if (!ok) {
      res.status(401).send(JSON.stringify("Wrong username"));
      return;
    }
    
    if (req.query.hasOwnProperty("metaData")){
      data = await getSegMetaData(req.query.segId)
      res.status(200).send(JSON.stringify(data));
      return;
    }
  }
  
  if (req.hasOwnProperty("body") && req.body.hasOwnProperty("metaData")){
    switch(req.body.metaData.Modality) {
      case 'RTSTRUCT':
        const ok = await checkAccess(username, "seg", req.body.metaData.segId)
    
        if (!ok) {
          res.status(401).send(JSON.stringify("Wrong username"));
          return;
        }
    
        await setSegMetaData(req.body.metaData)
        res.status(200).send({ done: true })
        break;
    }
  }
}

