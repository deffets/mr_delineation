
import { getSession } from '../../lib/iron'
import { } from '../../utils/db'
import * as ServersConfig from "./config.js"; //Definition of IP:port of the servers

const dicomHost = 'http://'+ ServersConfig.OrthancConfig.IP+':'+ ServersConfig.OrthancConfig.portAPI;

function RestApiGet(url){
  var options = {
    method : 'GET',
    uri : `${dicomHost}${url}`,
  };

  return requestPromise(options)
    .then(function(body){
      return Promise.resolve(JSON.parse(body));
    })
    .catch(function(err){
      console.log('Error on contacting server at', options.uri);
      return Promise.reject(err);
    });
}


// Parses a complete study and fills the payload with info from that study.
// That way, we are ready to parse multiple studies (at patient level)
function parseStudy(studyId, payload) {
  //var studyTags = RestApiGet('/studies/' .. studyId .. '/shared-tags?simplify')
  return RestApiGet('/studies/' + studyId)
    .then(function(orthancStudy){
      // parses all series in the study
      var seriesPromises = [];
      _.forEach(orthancStudy['Series'], function(seriesId){
        seriesPromises.push(parseSeries(seriesId, payload));
      })
      return Promise.all(seriesPromises);
    })
}


function parseSeries(seriesId, payload){
  console.log('Retrieving tags for series', seriesId);

  return Promise.coroutine(function* (){
    var seriesTags = yield RestApiGet('/series/' + seriesId + '/shared-tags?simplify');

    if (seriesTags['Modality'] == 'CT') {
      // this might be a CT or a CBCT
      if (seriesTags['ImageType'].search('CBCT') > -1) {  // this is a CBCT from IBA
        payload['cbcts'][seriesId] = yield parseCtSeries(seriesTags, 'IBA CBCT');
      }
      else if (seriesTags['ManufacturerModelName'].search('OBI Cone-beam CT') > -1) { // this is a CBCT from Varian
        payload['cbcts'][seriesId] = yield parseCtSeries(seriesTags, 'Varian CBCT');
      }
      else if (seriesTags['ManufacturerModelName'].search('RayStation') > -1) { // this is a CBCT from Raysearch
        payload['cbcts'][seriesId] = yield parseCtSeries(seriesTags, 'Raysearch CBCT');
      }
      else if (seriesTags['ImageType'].search('VCT') > -1) { // this is a Virtual CT
        payload['vcts'][seriesId] = yield parseCtSeries(seriesTags, 'VCT');
      }
      else {// this is a normal CT
        payload['patient']['PatientPosition'] = seriesTags['PatientPosition'];
        payload['cts'][seriesId] = yield parseCtSeries(seriesTags, 'CT');
      }
    }

    else if (seriesTags['Modality'] == 'RTDOSE') {
      var orthancSeriesInfo = yield RestApiGet('/series/' + seriesId);
      var doses = yield parseRtDoseSeries(seriesTags, orthancSeriesInfo);
      mergeTables(payload['doseMaps'], doses);
    }

    else if (seriesTags['Modality'] == 'RTSTRUCT') {
      // the RT struct is actualy a single instance.  /shared-tags will not return the hierarchical tags so let's take the tags from the first instance
      var orthancSeriesInfo = yield RestApiGet('/series/' + seriesId);
      var instanceId = orthancSeriesInfo['Instances'][0];
      var rtStructSeriesTags = yield RestApiGet('/instances/' + instanceId + '/tags?simplify');
      payload['structureSets'][instanceId] = yield parseRtStructSeries(rtStructSeriesTags);  // in this case, the key is the instanceId, not the seriesId
    }

    else if (seriesTags['Modality'] == 'REG') {
      // the RT struct is actualy a single instance.  /shared-tags will not return the hierarchical tags so let's take the tags from the first instance
      var orthancSeriesInfo = yield RestApiGet('/series/' + seriesId);
      var instanceId = orthancSeriesInfo['Instances'][0];
      var rtRegistrationTags = yield RestApiGet('/instances/' + instanceId + '/tags?simplify');
      payload['align'][instanceId] = yield parseRtRegistration(rtRegistrationTags);
        // in this case, the key is the instanceId, not the seriesId
    }

    else if (seriesTags['Modality'] == 'RTPLAN') {
      // the RT plan is actualy a single instance.  /shared-tags will not return the hierarchical tags so let's take the tags from the first instance
      var orthancSeriesInfo = yield RestApiGet('/series/' + seriesId);
      var planInstanceId = orthancSeriesInfo['Instances'][0];
      var rtPlanSeriesTags = yield RestApiGet('/instances/' + planInstanceId + '/tags?simplify');
      var plan = yield parseRtPlan(rtPlanSeriesTags);
      // this might be a TPS plan or a log plan
      console.log('planInstanceId == ) ',planInstanceId)
      console.log('rtPlanSeriesTags == ) ',rtPlanSeriesTags)
      console.log('Plan ==  ',plan)
      console.log('plan[RTPlanName]  ',plan['RTPlanName'])
      if (plan['RTPlanName'] != undefined && plan['RTPlanName'].search('log.b') > -1) { // plan created by the log2planUMCG python converter
        // in this case, the plan is saved in logplans the treatment is NOT created
        payload['logplans'][planInstanceId] = plan;  // in this case, the key is the instanceId, not the seriesId
      }
      else { // this is a TPS plan
        // in this case, all runs as before
        payload['machine'] = plan['machine'];
        payload['plans'][planInstanceId] = plan;  // in this case, the key is the instanceId, not the seriesId

        // create a treatment for each RT Plan
        var treatment = {
          plan: planInstanceId,
          customized: false,
          structureSet: plan['referenceStructureSetInstanceOrthancId'],
          doseMaps: [],
          ct: null
        };

        payload['treatments'].push(treatment);
      }
    }
    return Promise.resolve(true);
  })();
}


export default async function (req, res) {  
  const studyId = req.body.studyId;
  
  res.status(201).send(JSON.stringify("Received"));
  
  
}

