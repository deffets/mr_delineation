
import { getSession } from '../../lib/iron'
import { addPublicKey, checkAccess, getWorkflow, cleanUnsafe, deleteWorkflow } from '../../utils/db'

export default async function (req, res) {
  //TODO: Check for authorization
  let data = null
  
  let session = null;
  
  try {
    session = await getSession(req)
  }
  catch (e) {
    res.status(401).send(JSON.stringify(""));
    return;
  }
  
  if (!session) {
    res.status(401).send(JSON.stringify("User not authenticated"));
    return;
  }
  
  const username = session.username;
  
  if(req.query.hasOwnProperty("workflowId")){
    const ok = await checkAccess(username, "workflow", req.query.workflowId)
    
    if (!ok) {
      res.status(401).send(JSON.stringify("Wrong username"));
      return;
    }
    
    data = await getWorkflow(req.query.workflowId)
    
    res.status(200).send(JSON.stringify(data));
    return;
  }
  
  if(req.query.hasOwnProperty("cleanUnsafe")){
    const ok = await checkAccess(username, "clean", null)
    
    if (!ok) {
      res.status(401).send(JSON.stringify("Wrong username"));
      return;
    }
    
    let count = await cleanUnsafe()
    
    res.status(200).send(JSON.stringify(count));
    return;
  }
  
  if(req.query.hasOwnProperty("publicKey")){
    const ok = await checkAccess(username, "publicKey", null)
    
    if (!ok) {
      res.status(401).send(JSON.stringify("Wrong username"));
      return;
    }
    
    await addPublicKey(req.query.publicKey)
    
    res.status(200).send(JSON.stringify(""));
    return;
  }
  
  if(req.query.hasOwnProperty("deleteWorkflow")){
    const ok = await checkAccess(username, "deleteWorkflow", null)
    
    if (!ok) {
      res.status(401).send(JSON.stringify("Wrong username"));
      return;
    }
    
    let workflows = req.query.deleteWorkflow;
    
    if (!workflows)
      res.status(200).send(JSON.stringify(""));
      
    if (!Array.isArray(workflows))
      workflows = [workflows];
      
    for (let i=0 ; i<workflows.length; i++)
      await deleteWorkflow(workflows[i]);
    
    res.status(200).send(JSON.stringify(""));
    return;
  }
  
  res.status(401).send(JSON.stringify(""));
}

