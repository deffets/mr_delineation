import { getSession } from '../../lib/iron'
import { createUser } from '../../lib/user'

export default async function signup(req, res) {
  let session = null;
  
  try {
    session = await getSession(req)
  }
  catch (e) {
    res.status(401).send(JSON.stringify(""));
    return;
  }
  
  if (!session) {
    res.status(401).send(JSON.stringify("User not authenticated"));
    return;
  }
  
  const username = session.username;
  
  try {
    const ok = await checkAccess(username, "insertUser", null)
    
    if (!ok) {
      res.status(401).send(JSON.stringify("Wrong username"));
      return;
    }
    
    await createUser(req.body)
    res.status(200).send({ done: true })
  } catch (error) {
    console.error(error)
    res.status(500).end(error.message)
  }
}
