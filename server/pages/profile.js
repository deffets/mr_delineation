import { useUser } from '../lib/hooks'
import Layout from '../components/layout'
import { connectToDatabase, getUser } from '../utils/db'
import { getSession } from '../lib/iron'

export default function Profile ({ isConnected, admin }) {
  const user = useUser({ redirectTo: '/login' })

  return (
    <Layout admin={admin}>
      <h1>Profile</h1>
      {user && <p>Your session: {JSON.stringify(user)}</p>}
    </Layout>
  )
}

export async function getServerSideProps(context) {
  const { client } = await connectToDatabase()
  
  const isConnected = await client.isConnected() // Returns true or false
  
  const session = await getSession(context.req)
  
  let user = null
  if (session)
    user = await getUser(session.username)
    
  let admin = false
  if (user)
    admin = user.role=="admin"
  
  return {
    props: { isConnected, admin },
  }
}

