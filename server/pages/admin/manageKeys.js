
import React, { Component } from "react";
import AdminLayout from "../../components/adminLayout"
import { connectToDatabase, getUser } from '../../utils/db'
import { getSession } from '../../lib/iron'

export default function ManageKeys ({ isConnected }) {
  const handleSubmit = async function () {
    const publicKey = document.getElementById("publicKey").value;
    
    //TODO POST instead of GET
    let workflow = await fetch('/api/admin?publicKey='+publicKey, {method: 'GET'});
    
    document.getElementById("publicKey").value = "";
  }
  
  return(
    <AdminLayout>
      <div className="manageKeys">
        <h2>Add a public key</h2><br/>
        <textarea id="publicKey" name="publicKey" style={{width:"400px", height:"200px"}}/><br/>
        <button type="button" style={{width:"400px"}} onClick={()=>{handleSubmit()}}>Submit</button>
      </div>
      <style>{`
        .manageKeys {
          padding: 40px 40px 40px 40px;
          width: 100%;
          height: 720px;
          position: relative;
          display: flex;
          flex-direction: column;
          justify-content: start;
        }
      `}</style>
    </AdminLayout>
  )
}

export async function getServerSideProps(context) {
  const { client } = await connectToDatabase()
  
  const isConnected = await client.isConnected() // Returns true or false
  
  let tasks = []
  const session = await getSession(context.req)
  
  let user = null
  if (session)
    user = await getUser(session.username)
    
  let admin = false
  if (user)
    admin = user.role=="admin"
    
  //TODO: if not admin

  return {
    props: { isConnected },
  }
}

