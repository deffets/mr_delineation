
import React, { Component } from "react";
import { useUser } from '../../lib/hooks'
import Form from '../../components/form'
import AdminLayout from "../../components/adminLayout"
import { useState } from 'react'
import { useRouter } from 'next/router';

export default function AddUser() {
    const user = useUser({ redirectTo: '/login' })
    const [errorMsg, setErrorMsg] = useState('')
    const router = useRouter();
    
    async function handleSubmit(e) {
      e.preventDefault()

      if (errorMsg) setErrorMsg('')

      const body = {
        username: e.currentTarget.username.value,
        password: e.currentTarget.password.value,
        role: e.currentTarget.role.value,
      }

      if (body.password !== e.currentTarget.rpassword.value) {
        setErrorMsg(`The passwords don't match`)
        return
      }

      try {
        const res = await fetch('/api/signup', {
          method: 'POST',
          headers: { 'Content-Type': 'application/json' },
          body: JSON.stringify(body),
        })
        if (res.status === 200) {
        } else {
          throw new Error(await res.text())
        }
      } catch (error) {
        console.error('An unexpected error happened occurred:', error)
        setErrorMsg(error.message)
      }
    }
  
    return (
      <AdminLayout>
        <div className="addUser">
          <h1>User management</h1>

          {!user ? null : (
            <div>
              <h2>Add new user</h2>
              <Form isLogin={false} errorMessage={errorMsg} onSubmit={handleSubmit} />
            </div>
          )}
        </div>

        <style>{`
          .addUser {
            display: inline-block;
            width: 42rem;
            height: 100%;
            position: relative;
            padding-left: 40px;
          }
          .addUser li {
            margin-bottom: 0.5rem;
          }
          .addUser th, td {
            padding: 8px;
            text-align: left;
            border-bottom: 1px solid #ddd;
          }
          tr:hover {background-color:#f5f5f5;}s
        `}</style>
      </AdminLayout>
    )
}

