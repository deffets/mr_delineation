
import Router from 'next/router'
import React, { Component } from "react";
import styled, { createGlobalStyle } from 'styled-components'
import AdminLayout from "../../components/adminLayout"
import { connectToDatabase, getAllWorkflows, getUser } from '../../utils/db'
import { getSession } from '../../lib/iron'
import * as echarts from "echarts";

function workflowToData(workflow) {
  let parent = workflow[0].parents[0];
  
  let x=0, y=-100, data=[];
    
  for (let i=0; i<workflow.length; i++){
    const id = workflow[i]._id.toString();
    
    if (workflow[i].parents[0] != parent) {
      x += 100;
      y = 0;
    } else {
      y += 100;
    }
    
    const name = workflow[i].label;
    
    data.push({name, id, x, y});
    
    parent = workflow[i].parents[0];
  }
  
  return data;
}

function workflowToLinks(workflow, data) {
  let links = [];
  const symbolSize = [5, 20];
  const label = {show: false};
  const lineStyle = { width: 5, curveness: 0.2, color: "green"};
  
  for (let i=0; i<workflow.length; i++){
    let target = workflow[i]._id.toString();
    
    for (let j=0; j<workflow[i].parents.length; j++) {
      let source = workflow[i].parents[j].toString();
      
      links.push({source, target, label, symbolSize, lineStyle});
    
    }
  }
  
  return links;
}

export default function ManageWorkflows ({ isConnected, tasks }) {
  const workflowElId = "workflowId";
  
  const handleWorkflowClick = async function (worfklowId) {     
    let workflow = await fetch('/api/admin?workflowId='+worfklowId, {method: 'GET'});
    workflow = await workflow.json();
    
    const data = workflowToData(workflow);
    const links = workflowToLinks(workflow, data);
    
    const option = {
        title: {
            text: workflow[0].info
        },
        tooltip: {},
        animationDurationUpdate: 1500,
        animationEasingUpdate: 'quinticInOut',
        series: [
            {
                type: 'graph',
                layout: 'none',
                symbolSize: 50,
                roam: true,
                label: {
                    show: true
                },
                edgeSymbol: ['circle', 'arrow'],
                edgeSymbolSize: [4, 10],
                edgeLabel: {
                    fontSize: 20
                },
                data,
                links,
                lineStyle: {
                    opacity: 0.9,
                    width: 2,
                    curveness: 0
                }
            }
        ]
    };
    
    const myChart = echarts.init(document.getElementById(workflowElId));
    myChart.setOption(option);
    myChart.on('click', function (params) {
     Router.push({
        pathname: '/viewer',
        query: { workflowEntryId: params.data.id }
      });
    });
  }
  
  const cleanUnsafe = async function () {
    if (confirm("Are you sure that you want to delete permanently unused files? This cannot be undone. This operation is unsafe. It may delete data not yet linked to a workflow.")) {
      let count = await fetch('/api/admin?cleanUnsafe='+true, {method: 'GET'});
      count = await count.json();
      
       alert(count + " data were permanently deleted.");
    }
  }
  
  const deleteWorkflow = async function() {
    if (!tasks || !tasks.length)
      return;
      
    let queryString = "";
    
    for (let i=0; i<tasks.length; i++) {
      if (document.getElementById("checkbox"+tasks[i].workflowId).checked) {
        if (queryString.length)
          queryString += "&";
          
        queryString += "deleteWorkflow=" + tasks[i].workflowId;
      }
    }
    
    await fetch("/api/admin?"+queryString, {method: 'GET'});
    
    Router.push("/admin/manageWorkflows");
  }
  
  const toggleAll = function() {
    const selectAll = document.getElementById("selectAll").checked;
    
    for (let i=0; i<tasks.length; i++)
      document.getElementById("checkbox"+tasks[i].workflowId).checked = selectAll;
  }
   
  let tasksArray = null;
  
  if (tasks && tasks.length) {
    tasksArray = tasks.map(el => <tr onClick={()=>{handleWorkflowClick(el.workflowId)}}>
          <td><input type="checkbox" id={"checkbox"+el.workflowId}/></td>
          <td>{el.info}</td>
          <td>{el.BodyPartExamined}</td>
          <td>{el.PatientSex}</td>
          <td>{el.PatientAge}</td>
          <td>{el.date}</td>
          <td>{el.workflowId}</td>
          <style jsx>{`
          td {
            cursor: pointer;
          }
        `}</style>
      </tr>)
    }
      
    return (
      <AdminLayout>
        <div className="worflowLayout">
          <div className="workflowList">
            <table>
              <tr>
                <td><button type="button" onClick={()=>{deleteWorkflow()}}>Delete workflow entry</button></td>
                <td><button type="button" onClick={()=>{cleanUnsafe()}}>Clean unused data (unsafe)</button></td>
              </tr>
            </table>
            <h2>All workflows</h2>
            <th><input type="checkbox" id="selectAll" onClick={()=>{toggleAll()}}/></th><th>Description</th><th>Body part examined</th><th>Sex</th><th>Age</th><th>Date</th><th>Workflow id</th>
            {tasksArray}
          </div>
          <div className="workflow" id={workflowElId}/>
        </div>
        <style>{`
        .worflowLayout {
          width: 100%;
          height: 720px;
          position: relative;
          display: flex;
          flex-direction: row;
          justify-content: start;
        }
        .workflowList {
          width: 45%;
          height: 100%;
          position: relative;
          display: block;
        }
        .workflow {
          width: 45%;
          height: 100%;
          position: relative;
          display: block;
        }
      `}</style>
      </AdminLayout>
    );
}
  
export async function getServerSideProps(context) {
  const { client } = await connectToDatabase()
  
  const isConnected = await client.isConnected() // Returns true or false
  
  let tasks = []
  const session = await getSession(context.req)
  
  let user = null
  if (session)
    user = await getUser(session.username)
    
  let admin = false
  if (user)
    admin = user.role=="admin"
    
  //TODO: if not admin
  
  if (session)
    tasks = await getAllWorkflows()
    
  if (tasks && tasks.length)
    for (let i=0; i<tasks.length; i++)
      tasks[i] = {
        workflowId:  tasks[i].workflowId.toString(),
        info: tasks[i].info, 
        BodyPartExamined: tasks[i].BodyPartExamined, 
        PatientSex: tasks[i].PatientSex,
        PatientAge: tasks[i].PatientAge,
        date: tasks[i].date.toString(),
      }

  return {
    props: { isConnected, tasks },
  }
}

