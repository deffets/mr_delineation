
import React from 'react'
import Router from 'next/router'
import AdminLayout from "../components/adminLayout.js"

export default class DCMVAdminiewer extends React.Component {
  constructor(props) {
    super(props)
  }
  
  componentDidMount() {
    Router.push("/admin/manageWorkflows");
  }
  
  render() {
    return (
      <AdminLayout/>
    )
  }
}

