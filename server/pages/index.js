import { useUser } from '../lib/hooks'
import Layout from '../components/layout'
import { useState, setState } from 'react'
import { getSession } from '../lib/iron'
import { connectToDatabase, getWorkflowEntries, getUser } from '../utils/db'
import { useRouter } from 'next/router';

export default function Home({ isConnected, tasks, admin }) {
  const user = useUser()
  const [errorMsg, setErrorMsg] = useState('')
  const router = useRouter();

  async function handleWorkflowClick(workflowEntryId) {
    router.push({
        pathname: '/viewer',
        query: { workflowEntryId }
      });
      
    /*
    const body = {
      workflowEntryId,
    }
    
    try {
      const res = await fetch('/api/workflow', {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(body),
      })
      if (res.status === 200) {
        console.log(await res.json())
      } else {
        throw new Error(await res.text())
      }
    } catch (error) {
      console.error('An unexpected error occurred:', error)
    }*/
  }
  
  if (tasks && tasks.length)
    tasks = tasks.map(el => <tr onClick={()=>{handleWorkflowClick(el._id)}}>
          <td>{el.info}</td>
          <td>{el.BodyPartExamined}</td>
          <td>{el.PatientSex}</td>
          <td>{el.PatientAge}</td>
          <td>{el.date}</td>
          <style jsx>{`
          td {
            cursor: pointer;
          }
        `}</style>
      </tr>)
  
  return (
    <Layout admin={admin}>
      <div className="homeLayout">
        <div className="home">
          <h1>List of MR data</h1>
          
          {isConnected ? null : 
            (
              <h2 className="subtitle">
                Server error: Database connection failed.
              </h2>
            )
          }
          
          {!user ? null : (
            <div className="workflowList">
              <h2>Active workflows</h2>
              <th>Description</th><th>Body part examined</th><th>Sex</th><th>Age</th><th>Date</th>
              {tasks}
            </div>
          )}
        </div>
      </div>
      <style>{`
        .homeLayout {
          width: 100%;
          height: 100%;
          position: relative;
          display: flex;
          justify-content: center;
        }
        
        .home {
          display: inline-block;
          width: 42rem;
          height: 100%;
          position: relative;
        }
        
        .home .workflowList {
          width: 42rem;
          height: 100%;
          position: relative;
        }
        .workflowList th, td {
          padding: 8px;
          text-align: left;
          border-bottom: 1px solid #ddd;
        }
        tr:hover {background-color:#f5f5f5;}s
      `}</style>
    </Layout>
  )
}
  
export async function getServerSideProps(context) {
  const { client } = await connectToDatabase()
  
  const isConnected = await client.isConnected() // Returns true or false
  
  let tasks = []
  const session = await getSession(context.req)
  if (session)
    tasks = await getWorkflowEntries(session.username)
    
  if (tasks && tasks.length)
    for (let i=0; i<tasks.length; i++)
      tasks[i] = {
        _id:  tasks[i]._id.toString(),
        info: tasks[i].info, 
        BodyPartExamined: tasks[i].BodyPartExamined, 
        PatientSex: tasks[i].PatientSex,
        PatientAge: tasks[i].PatientAge,
        date: tasks[i].date.toString(),
      }
  
  let user = null
  if (session)
    user = await getUser(session.username)
    
  let admin = false
  if (user)
    admin = user.role=="admin"
  
  return {
    props: { isConnected, tasks, admin },
  }
}

