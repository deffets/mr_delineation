
import { getUser as findUserDB, insertUser } from '../utils/db'
import crypto from 'crypto'


export async function createUser({ username, password, role }) {  
  if (await findUserDB(username))
    throw new Error ("Username already exists");
  
  const salt = crypto.randomBytes(16).toString('hex')
  const hash = crypto.pbkdf2Sync(password, salt, 1000, 64, 'sha512').toString('hex')
  const user = await insertUser(username, salt, hash, role)

  return { username, createdAt: Date.now() }
}

export async function findUser({ username, password }) {
  const user = await findUserDB(username)
  
  if (!user)
    throw new Error("User does not exist");
    
  const hash = crypto.pbkdf2Sync(password, user.salt, 1000, 64, 'sha512').toString('hex')
  const passwordsMatch = user.hash === hash
  
  if (!passwordsMatch)
    throw new Error("Invalid password");
  
  return { username, createdAt: Date.now() }
}
