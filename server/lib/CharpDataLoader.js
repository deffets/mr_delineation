
export default class CharpDataLoader {
  static instance = null;
  url = null;
  enaled = true;
  runningNb = 0;
  
  enable() {
    this.enaled = true;
  }
  
  async disable() {
    this.enaled = false;
    
    await this.waitUntilIsDisabled();
  }
  
  async waitUntilIsDisabled() {
    const sleep = (ms) => new Promise(resolve => setTimeout(resolve, ms));
    
    while(this.runningNb) {
      await sleep(50);
    }
    if(!this.enaled) // if was intentionnaly disable, wait 10 ms more to be sure return statement occured.
      sleep(10);
  }
  
  setURL(url) {
    if(url==this.url)
      return;
      
    this.url = url;
  }
  
  static getInstance(){
    if (!CharpDataLoader.instance)
      CharpDataLoader.instance = new CharpDataLoader();
      
    return this.instance;
  }

  async getInstanceData(seriesId, instanceId) {
    if (!this.enaled)
      return null;
      
    if (!instanceId) {
      console.log('Error: invalid instanceId')
      return;
    }
      
    this.runningNb++;
    
    const raw = await fetch(this.url+'?seriesId='+seriesId+"&instanceId="+instanceId, {method: 'GET'})
    if (raw.status==401) {
      alert("You do not have the correct access right.")
      return null;
    }
    
    const aB = await raw.arrayBuffer();
    const data = new Int16Array(aB)
    
    this.runningNb--;
    return data;
  }

  async getInstancesDataFromSeriesMetaData(seriesMetaData, progressFunc) {
    this.runningNb++;
    
    let data = new Int16Array(seriesMetaData[0].Rows*seriesMetaData[0].Columns*seriesMetaData.length);
    
    // metadata already sorted by getSeriesMetaData
    let ind = 0;
    for (let i=0; i<seriesMetaData.length; i++) {
      if (!this.enaled) {
        this.runningNb--;
        
        return null;
      }
      
      console.log('Loading instance ' + (i+1) + '/' + seriesMetaData.length);
      
      let instanceMetada = seriesMetaData[i];
      let instanceData = await this.getInstanceData(instanceMetada.seriesId, instanceMetada.instanceId);
      data.set(instanceData, ind);
      ind = ind+instanceData.length;
      
      progressFunc((i+1)/seriesMetaData.length);
    }
    
    this.runningNb--;
    return data;
  }

  async getSeriesData(dontcare, seriesId, progressFunc) {
    if (!this.enaled)
      return {data: null, metaData:null};
    
    if (!seriesId) {
      console.log('Error: invalid seriesId')
      return;
    }
    
    this.runningNb++;
    
    const seriesMetaData = await this.getSeriesMetaData(null, seriesId);
    const data = await this.getInstancesDataFromSeriesMetaData(seriesMetaData, progressFunc);
    
    this.runningNb--;
    return {data, metaData:seriesMetaData};
  }

  async getSeriesMetaData(dontcare, seriesId) {
    if (!this.enaled)
      return null;
    
    if (!seriesId) {
      console.log('Error: invalid seriesId')
      return;
    }
    
    this.runningNb++;
    
    let meta = await fetch(this.url+"?seriesId="+seriesId+"&metaData=true", {method: 'GET'})
    if (meta.status==401) {
      alert("You do not have the correct access right.")
      return null;
    }
      
    const metaData = await meta.json()
    
    this.runningNb--;
    return metaData;
  }
  
  async getSEGData(dontcare, dontcare2, segId) {
    if (!this.enaled)
      return null;
    
    if (!segId) {
      console.log('Error: invalid segId')
      return;
    }
    
    this.runningNb++;
    
    let meta = await fetch(this.url+"?segId="+segId+"&metaData=true", {method: 'GET'})
    if (meta.status==401) {
      alert("You do not have the correct access right.")
      return null;
    }
      
    const metaData = await meta.json()
    
    console.log(metaData)
    
    this.runningNb--;
    return metaData;
  }
  
  async getStudyMetaData(worfklowId) {
    if (!this.enaled)
      return null;
      
    if (!worfklowId) {
      console.log('Error: invalid worfklowId')
      return;
    }
      
      
    this.runningNb++;
    
    let workflow = await fetch(this.url+'?workflowEntryId='+worfklowId, {method: 'GET'})
    if (workflow.status==401) {
      alert("You do not have the correct access right.")
      return null;
    }
      
    workflow = await workflow.json()
    
    this.runningNb--;
    return workflow;
  }
  
  sendSEGData(metaData) {
    console.log('Sending SEG metadata')
    
    delete metaData._id
    
    for (let segNb=0; segNb<metaData.ROIContourSequence.length; segNb++) {
      if(!metaData.ROIContourSequence[segNb].changed)
        metaData.ROIContourSequence[segNb].ContourSequence = []
    }
    
    const body = {
      metaData,
      data: null
    }
    
    fetch(this.url, {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(body),
    }).then(res => console.log(res))
    
    console.log('Done Sending SEG metadata')
    
    // TODO check answer
  }
}

