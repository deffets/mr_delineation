
if __name__ == "__main__":
  import sys, os
  from automation_api.api import DB
  import copy
  import numpy as np
  from matplotlib import pyplot as plt
  
  
  dataPath = "/home/sylvain/Documents/Gitlab/mr_delineation/data"
  dbName = 'spine1'
  
  # Load initial data
  db = DB(dataPath=dataPath, dbName=dbName)
  
  #db.insertUser("sylvain", "aaaa", "admin")
  
  
  series = ["/home/sylvain/Documents/Data/CIA/PDMR-292921-168-R/292921-168-R-1404/2.25.10404001905468495441217502713666726991/2.25.53150790375494714794192462315596304418/1-01.dcm"
    #"/home/sylvain/Documents/Data/CIA/PDMR-292921-168-R/292921-168-R-1404/2.25.166810139705163480126303278160584331987/2.25.34488972863903273587615289396271229718/1-01.dcm",
    #"/home/sylvain/Documents/Data/CIA/PDMR-292921-168-R/292921-168-R-1404/2.25.302663032735478633565561232177315735711/2.25.214822185761093111184378443394467805037/1-01.dcm"
    ]
  
  # Load initial data
  db = DB(dataPath=dataPath, dbName=dbName)
  
  seriesInfo = db.insertDcmSeries(series)
  
  seriesIds = seriesInfo["id"]
  seriesMeta = seriesInfo["meta"]
  seriesData = seriesInfo["data"]
  
  studyMeta = seriesMeta[0][0]
  
  
  seriesMeta = seriesMeta[0]
  
  ImagePositionPatient = list(map(float, seriesMeta[0]["ImagePositionPatient"]))
  PixelSpacing = list(map(float, seriesMeta[0]["PixelSpacing"]))
  SliceThickness = seriesMeta[0]["SliceThickness"]
  Rows = seriesMeta[0]["Rows"]
  Columns = seriesMeta[0]["Columns"]
  Slices = len(seriesMeta)
  ImageOrientationPatient = list(map(float, seriesMeta[0]["ImageOrientationPatient"]))
  StudyDate = str(seriesMeta[0]["StudyDate"])
  SeriesDate = str(seriesMeta[0]["SeriesDate"])
  Manufacturer = str(seriesMeta[0]["Manufacturer"])
  PatientID = str(seriesMeta[0]["PatientID"])
  StudyInstanceUID = str(seriesMeta[0]["StudyInstanceUID"] + "_structStudyUID")
  SeriesInstanceUID = str(seriesMeta[0]["SeriesInstanceUID"]+ "_structSeriesUID") 
  StructureSetROISequence = [ {
        "ROINumber": str(1),
        "ROIName": str("Spine")
      }]
  ROIContourSequence = [ {
      "ROIDisplayColor": [0, 0, 0],
      "ContourSequence": [ {
        "NumberOfContourPoints": 3,
        "ContourData": [0, 0, 0, 0, 1, 0, 1, 0, 0]
      }]
    }]
  structMeta = {
      "modalities": str("RTSTRUCT"),
      "modality": str("RTSTRUCT"),
      "Modality": str("RTSTRUCT"),
      "StudyDate": StudyDate,
      "SeriesDate": SeriesDate,
      "Manufacturer": Manufacturer,
      "PatientName": "",
      "PatientID": PatientID,
      "BodyPartExamined": "",
      "StudyInstanceUID": StudyInstanceUID,
      "SeriesInstanceUID": SeriesInstanceUID,
      "StructureSetROISequence" : StructureSetROISequence,
      "ROIContourSequence": ROIContourSequence,
      "SeriesDescription": "",
      "Rows": Rows, #This is non standard
      "Columns": Columns, #This is non standard
      "Slices": Slices, #This is non standard
      "SliceThickness": SliceThickness, #This is non standard
      "PixelSpacing": PixelSpacing, #This is non standard
      "ImagePositionPatient": ImagePositionPatient, #This is non standard
      "ImageOrientationPatient": ImageOrientationPatient #This is non standard
    }
    
  segId = db.insertData(structMeta)["id"]
    
  info = "TCIA"
  label = "Initial data"
  rootId = db.insertWorkflowEntry(seriesIds, segId, None, studyMeta, info, None, label)
  
  # User reviews
  label = "Sylvain"
  delinat = db.insertWorkflowEntry(seriesIds, segId, "sylvain", studyMeta, info, [rootId], label)
  
