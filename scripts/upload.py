
if __name__ == "__main__":
  import sys, os
  from automation_api.api import DB
  import copy
  import numpy as np
  from matplotlib import pyplot as plt
  
  
  dataPath = "/home/sylvain/Documents/Gitlab/mr_delineation/data"
  dbName = 'spine1'
  
  # Load initial data
  db = DB(dataPath=dataPath, dbName=dbName)
  
  #db.insertUser("sylvain", "aaaa", "admin")
  
  data_name = "AI-00003"
  
  data_np = np.load("/home/sylvain/Documents/Data/spine_st-luc/" + data_name + "/npy/mri_w.npy")
  
  ctMeta = []
  for i in range(data_np.shape[-1]):
    col = {
      "ImagePositionPatient": [0, 0, 0],
      "Manufacturer": str("Unknown"),
      "PatientName": data_name,
      "PatientID": "Anon",
      "PatientSex": "Anon",
      "PatientAge": "Anon",
      "PatientWeight": "Anon",
      "BodyPartExamined": "Anon",
      "SliceThickness": 1,
      "SliceLocation": i,
      "PatientPosition": "",
      "StudyInstanceUID": data_name,
      "SeriesInstanceUID": data_name + str(i),
      "ImageOrientationPatient": [1, 0, 0, 0, 1, 0],
      "Rows": data_np.shape[0],
      "Columns": data_np.shape[1],
      "PixelSpacing": [1, 1],
      "BitsAllocated": 16,
      "BitsStored": 16*data_np.shape[0]*data_np.shape[1]*data_np.shape[2],
      "HighBit": "",
      "RescaleIntercept": -1024,
      "RescaleSlope": 1,
      "modalities": "MR",
      "modality": "MR",
      "Modality": "MR",
      "SeriesDescription": ""
    }
    ctMeta.append(col)
    
  res = db.insertData(ctMeta, data_np)
  
  seriesIds = [res["id"]]
  studyMeta = res["meta"][0]
  
  
  data_np = np.load("/home/sylvain/Documents/Data/spine_st-luc/" + data_name + "/npy/mri_op.npy")
  
  ctMeta = []
  for i in range(data_np.shape[-1]):
    col = {
      "ImagePositionPatient": [0, 0, 0],
      "Manufacturer": str("Unknown"),
      "PatientName": data_name,
      "PatientID": "Anon",
      "PatientSex": "Anon",
      "PatientAge": "Anon",
      "PatientWeight": "Anon",
      "BodyPartExamined": "Anon",
      "SliceThickness": 1,
      "SliceLocation": i,
      "PatientPosition": "",
      "StudyInstanceUID": data_name,
      "SeriesInstanceUID": data_name + str(i),
      "ImageOrientationPatient": [1, 0, 0, 0, 1, 0],
      "Rows": data_np.shape[0],
      "Columns": data_np.shape[1],
      "PixelSpacing": [1, 1],
      "BitsAllocated": 16,
      "BitsStored": 16*data_np.shape[0]*data_np.shape[1]*data_np.shape[2],
      "HighBit": "",
      "RescaleIntercept": -1024,
      "RescaleSlope": 1,
      "modalities": "MR",
      "modality": "MR",
      "Modality": "MR",
      "SeriesDescription": ""
    }
    ctMeta.append(col)
    
  res = db.insertData(ctMeta, data_np)    
  seriesIds.append(res["id"])
    
  data_np = np.load("/home/sylvain/Documents/Data/spine_st-luc/" + data_name + "/npy/mri_ip.npy")
  
  ctMeta = []
  for i in range(data_np.shape[-1]):
    col = {
      "ImagePositionPatient": [0, 0, 0],
      "Manufacturer": str("Unknown"),
      "PatientName": data_name,
      "PatientID": "Anon",
      "PatientSex": "Anon",
      "PatientAge": "Anon",
      "PatientWeight": "Anon",
      "BodyPartExamined": "Anon",
      "SliceThickness": 1,
      "SliceLocation": i,
      "PatientPosition": "",
      "StudyInstanceUID": data_name,
      "SeriesInstanceUID": data_name + str(i),
      "ImageOrientationPatient": [1, 0, 0, 0, 1, 0],
      "Rows": data_np.shape[0],
      "Columns": data_np.shape[1],
      "PixelSpacing": [1, 1],
      "BitsAllocated": 16,
      "BitsStored": 16*data_np.shape[0]*data_np.shape[1]*data_np.shape[2],
      "HighBit": "",
      "RescaleIntercept": -1024,
      "RescaleSlope": 1,
      "modalities": "MR",
      "modality": "MR",
      "Modality": "MR",
      "SeriesDescription": ""
    }
    ctMeta.append(col)
  
  res = db.insertData(ctMeta, data_np)    
  seriesIds.append(res["id"])
  
  
  seriesMeta = ctMeta
  
  ImagePositionPatient = list(map(float, seriesMeta[0]["ImagePositionPatient"]))
  PixelSpacing = list(map(float, seriesMeta[0]["PixelSpacing"]))
  SliceThickness = seriesMeta[0]["SliceThickness"]
  Rows = seriesMeta[0]["Rows"]
  Columns = seriesMeta[0]["Columns"]
  Slices = len(seriesMeta)
  ImageOrientationPatient = list(map(float, seriesMeta[0]["ImageOrientationPatient"]))
  StudyDate = ""
  SeriesDate = ""
  Manufacturer = str(seriesMeta[0]["Manufacturer"])
  PatientID = str(seriesMeta[0]["PatientID"])
  StudyInstanceUID = str(seriesMeta[0]["StudyInstanceUID"] + "_structStudyUID")
  SeriesInstanceUID = str(seriesMeta[0]["SeriesInstanceUID"]+ "_structSeriesUID") 
  StructureSetROISequence = [ {
        "ROINumber": str(1),
        "ROIName": str("Spine")
      }]
  ROIContourSequence = [ {
      "ROIDisplayColor": [0, 0, 0],
      "ContourSequence": [ {
        "NumberOfContourPoints": 3,
        "ContourData": [0, 0, 0, 0, 1, 0, 1, 0, 0]
      }]
    }]
  structMeta = {
      "modalities": str("RTSTRUCT"),
      "modality": str("RTSTRUCT"),
      "Modality": str("RTSTRUCT"),
      "StudyDate": StudyDate,
      "SeriesDate": SeriesDate,
      "Manufacturer": Manufacturer,
      "PatientName": "",
      "PatientID": PatientID,
      "BodyPartExamined": "",
      "StudyInstanceUID": StudyInstanceUID,
      "SeriesInstanceUID": SeriesInstanceUID,
      "StructureSetROISequence" : StructureSetROISequence,
      "ROIContourSequence": ROIContourSequence,
      "SeriesDescription": "",
      "Rows": Rows, #This is non standard
      "Columns": Columns, #This is non standard
      "Slices": Slices, #This is non standard
      "SliceThickness": SliceThickness, #This is non standard
      "PixelSpacing": PixelSpacing, #This is non standard
      "ImagePositionPatient": ImagePositionPatient, #This is non standard
      "ImageOrientationPatient": ImageOrientationPatient #This is non standard
    }
    
  segId = db.insertData(structMeta)["id"]
  
  
  info = data_name
  label = "Initial data"
  rootId = db.insertWorkflowEntry(seriesIds, segId, None, studyMeta, info, None, label)
  
  # User reviews
  label = "Sylvain"
  delinat = db.insertWorkflowEntry(seriesIds, segId, "sylvain", studyMeta, info, [rootId], label)
  
